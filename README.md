# Php Mapping Merge
ty-mapping-merge; package is multiple data with do mapping and merge results.

Object and Array usable.  

For Example:  
Post and User relation map and merge.  
Map: Relation with key, Post (fromUserId) <==> User(id)  
Merge: Related data merge of with selected field (fromUser)  

```sh
// post data
$posts = array(
    array('id'=> 1, 'title'=> 'happy years 2018', 'fromUserId'=> 1),
    array('id'=> 2, 'title'=> 'i want to see you', 'fromUserId'=> 1),
    array('id'=> 3, 'title'=> 'Champion RealMadrid', 'fromUserId'=> 2),
    array('id'=> 4, 'title'=> 'love php', 'fromUserId'=> 3),
);

// user data
$users = array(
    array('id'=> 1, 'name'=> 'George', 'avatar'=> 'user1.jpg'),
    array('id'=> 2, 'name'=> 'Jack', 'avatar'=> 'user2.jpg'),
    array('id'=> 3, 'name'=> 'Oscar', 'avatar'=> 'user3.jpg'),
);

```

Result:
```sh
Array
(
    [0] => Array
        (
            [id] => 1
            [title] => happy years 2018
            [fromUserId] => 1
            [fromUser] => Array
                (
                    [id] => 1
                    [name] => George
                    [avatar] => user1.jpg
                )

        )
)        
```

# Use Case
Different data source merge at one result  
Relational data sources create or merge  
Create data by Microservice results, combine  
Aggregate data for multiple results  

# Microservice 
For example social app, microservice architecture:  
- Post Microservice
- User Microservice
- Like Microservice
- Comment Microservice
- Metric Microservice
etc.

Step by step data aggregate  
```sh
posts = postService->getPosts();
ids = posts.extractIds();
fromUserIds = posts.extractFromUserIds();

// get result sets by per microservice
users = userService->getByIds(fromUserIds);
likes = likeService->getByIds(ids);
comments = commentService->getByIds(ids);
metrics = metricService->getByIds(ids);
```

Data map, aggregate, combine, merge, join?  
Use to TyMappingMerge.  

# How To Use
```sh
/* 
$posts = array( ... );
$users = array( ... );
... defines codes ...
*/

// option user
$optionUser = new MapOption();
$optionUser->setVar($users);
$optionUser->setProp('fromUser');
$optionUser->setMapField('fromUserId');
$optionUser->setTargetMapField('id');

// set options
$options[] = $optionUser;

// mapping
$mapping = new OptionMapping();
$mapping->setOptions($options);
$result = $mapping->map($posts);
```
Look at test directory for social app example

# Doc

__MapOption__: multiple map and merge definitions.  

MapOption functions:  
->setVar($targetVar) required: target variable for relation data  
->setProp('viewField') required: add target results   
->setMapField('sourceRelationField') required: source relation key field  
->setTargetMapField('targetRelationId') optional: is empty then use source map field  
->setSetType('add') optional: item add type (set|add|bool) default: set
->setSelects(array('id', 'message')) optional: select visible fields  


__OptionMapping__: main map and merge operations.  

OptionMapping functions:  
->setOptions($defineOptionList) required: map rule definitions  
->map($mainData) required: main source data  

# Features
- Establish a relationship between the recipients
- Data merge with custom field
- Array or Object usable
- Select visible fields
- Easy use

# Examples and Tests
Look at Tests folder

# Support Types:
Array and Object

# Todos
Nested data  


# Requires & Dependency
PHP Version >= 5.4

# Version
v1.1.0

# License
MIT

# Contact
Questions, suggestions, comments:

Tahsin Yüksel info@tahsinyuksel.com