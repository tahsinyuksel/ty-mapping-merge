<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/04/18
 * Time: 23:35
 */

namespace TyMappingMerge;


class MapOption
{
    protected $var;
    protected $prop;
    protected $mapField;
    protected $targetMapField;
    protected $setType = 'set';
    protected $selects = array();

    /**
     * @return mixed
     */
    public function getVar()
    {
        return $this->var;
    }

    /**
     * @param mixed $var
     */
    public function setVar($var)
    {
        $this->var = $var;
    }

    /**
     * @return mixed
     */
    public function getProp()
    {
        return $this->prop;
    }

    /**
     * @param mixed $prop
     */
    public function setProp($prop)
    {
        $this->prop = $prop;
    }

    /**
     * @return mixed
     */
    public function getMapField()
    {
        return $this->mapField;
    }

    /**
     * @param mixed $mapField
     */
    public function setMapField($mapField)
    {
        $this->mapField = $mapField;
    }

    /**
     * @return mixed
     */
    public function getTargetMapField()
    {
        return ($this->targetMapField != '') ? $this->targetMapField : $this->getMapField();
    }

    /**
     * @param mixed $targetMapField
     */
    public function setTargetMapField($targetMapField)
    {
        $this->targetMapField = $targetMapField;
    }

    /**
     * @return string
     */
    public function getSetType()
    {
        return $this->setType;
    }

    /**
     * @param string $setType
     */
    public function setSetType($setType)
    {
        $this->setType = $setType;
    }

    /**
     * @return array
     */
    public function getSelects()
    {
        return $this->selects;
    }

    /**
     * @param array $selects
     */
    public function setSelects($selects)
    {
        $this->selects = $selects;
    }
}