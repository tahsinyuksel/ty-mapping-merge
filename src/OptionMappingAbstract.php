<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/05/18
 * Time: 00:52
 */

namespace TyMappingMerge;


abstract class OptionMappingAbstract
{
    public function getVarFactory($var)
    {
        if(is_array($var)) {
            return new MappingArrayVar($var);
        } else if(is_object($var)) {
            return new MappingObjectVar($var);
        }
        throw new \Exception('Variable type not support');
    }

    protected $options = array();

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function extractFields($var, $field) {
        if(! is_array($var)) {
            return array();
        }

        $result = array();
        foreach($var as $value) {
            if(is_array($value) && isset($value[$field])) {
                $result[] = $value[$field];
            }
        }

        return $result;
    }

    abstract public function map(array $data);
}