<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/05/18
 * Time: 00:36
 */

namespace TyMappingMerge;


abstract class MappingVarAbstract
{
    protected $var;

    /**
     * MapVarAbstract constructor.
     * @param $var
     */
    public function __construct($var)
    {
        $this->var = $var;
    }

    abstract public function getValue($field);
    abstract public function setValue($field, $value);
    abstract public function addValue($field, $value);

    /**
     * @return mixed
     */
    public function getVar()
    {
        return $this->var;
    }

    /**
     * @param mixed $var
     */
    public function setVar($var)
    {
        $this->var = $var;
    }

}