<?php

// require bootstrap
require_once '../bootstrap.php';

use TyMappingMerge\OptionMapping;
use TyMappingMerge\MapOption;

// variables
$source = array(
    array('id'=> 1, 'title'=> 'aa'),
    array('id'=> 2, 'title'=> 'bb'),
    array('id'=> 3, 'title'=> 'cc'),
);

$target = array(
    array('id'=> 1, 'from'=> 'George', 'to'=> 2),
    array('id'=> 2, 'from'=> 'Jack', 'to'=> 3),
    array('id'=> 3, 'from'=> 'Oscar', 'to'=> 1),
);

// object variable
$a = new \stdClass();
$a->code = 2;
$a->sku = 'abc';

// mapping options
$option = new MapOption();
$option->setVar($target);
$option->setProp('isLike');
$option->setMapField('id');
$option->setTargetMapField('id');

$option2 = new MapOption();
$option2->setVar($a);
$option2->setProp('promotionProduct');
$option2->setMapField('id');
$option2->setTargetMapField('code');

// mapping
$mapping = new OptionMapping();
$mapping->setOptions(array($option, $option2));
$result = $mapping->map($source);

echo '<pre>';
print_r($result);

exit('-end-');