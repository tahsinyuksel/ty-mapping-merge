<?php

// require bootstrap
require_once '../bootstrap.php';

use TyMappingMerge\OptionMapping;
use TyMappingMerge\MapOption;

// variables
$posts = array(
    array('id'=> 1, 'title'=> 'happy years 2018', 'fromUserId'=> 1),
    array('id'=> 2, 'title'=> 'i want to see you', 'fromUserId'=> 1),
    array('id'=> 3, 'title'=> 'Champion RealMadrid', 'fromUserId'=> 2),
    array('id'=> 4, 'title'=> 'love php', 'fromUserId'=> 3),
);

$users = array(
    array('id'=> 1, 'name'=> 'George', 'avatar'=> 'user1.jpg'),
    array('id'=> 2, 'name'=> 'Jack', 'avatar'=> 'user2.jpg'),
    array('id'=> 3, 'name'=> 'Oscar', 'avatar'=> 'user3.jpg'),
);

$likes = array(
    array('id'=> 1, 'fromUserId'=> 3, 'toId'=> 2),
    array('id'=> 2, 'fromUserId'=> 1, 'toId'=> 3),
    array('id'=> 3, 'fromUserId'=> 2, 'toId'=> 1),
);

$comments = array(
    array('id'=> 1, 'byUserId'=> 1, 'toId'=> 1, 'message'=> 'oh right'),
    array('id'=> 2, 'byUserId'=> 2, 'toId'=> 1, 'message'=> 'interesting, :)'),
    array('id'=> 3, 'byUserId'=> 3, 'toId'=> 2, 'message'=> 'thank you baby'),
);

// object variable
$metric = new \stdClass();
$metric->id = 1;
$metric->viewCount = 22;
$metric->clickCount = 5;
$metric->level = 'basic';

// define mapping options

// option user
$optionUser = new MapOption();
$optionUser->setVar($users);
$optionUser->setProp('fromUser');
$optionUser->setMapField('fromUserId');
$optionUser->setTargetMapField('id');

// option comments
$optionComment = new MapOption();
$optionComment->setVar($comments);
$optionComment->setProp('comments');
$optionComment->setMapField('id');
$optionComment->setTargetMapField('toId');
$optionComment->setSetType('add');
$optionComment->setSelects(array('id', 'message'));

// option like 
$optionLike = new MapOption();
$optionLike->setVar($likes);
$optionLike->setProp('likeData');
$optionLike->setMapField('id');
$optionLike->setTargetMapField('toId');
$optionLike->setSetType('add');

// option metric 
$optionMetric = new MapOption();
$optionMetric->setVar($metric);
$optionMetric->setProp('metric');
$optionMetric->setMapField('id');
$optionMetric->setTargetMapField('id');
$optionMetric->setSelects(array('id', 'clickCount', 'viewCount'));

// set options
$options[] = $optionUser;
$options[] = $optionComment;
$options[] = $optionLike;
$options[] = $optionMetric;

// mapping
$mapping = new OptionMapping();
$mapping->setOptions($options);
$result = $mapping->map($posts);

echo '<pre>';
print_r($result);

exit('-end-');