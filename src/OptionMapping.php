<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/04/18
 * Time: 23:36
 */

namespace TyMappingMerge;


class OptionMapping extends OptionMappingAbstract
{
    public function map(array $data)
    {
        foreach($data as $key => $value) {

            $value = $this->getVarFactory($value);

            /** @var MapOption $option */
            foreach($this->getOptions() as $option) {

                foreach ( (!is_array($option->getVar()) ? array($option->getVar()) : $option->getVar()) as $targetVar) {

                    $targetVar = $this->getVarFactory($targetVar);

                    if($value->getValue($option->getMapField()) === $targetVar->getValue($option->getTargetMapField())) {

                        if($option->getSetType() == 'set') {
                            $data[$key] = $value->setValue($option->getProp(), $targetVar->getWithSelect($option->getSelects()));
                        } else if($option->getSetType() == 'bool') {
                            $data[$key] = $value->setValue($option->getProp(), $targetVar->getVar() != null);
                        } else {
                            $data[$key] = $value->addValue($option->getProp(), $targetVar->getWithSelect($option->getSelects()));    
                        }
                        
                    }
                }
            }
        }
        return $data;
    }
}