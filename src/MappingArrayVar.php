<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/05/18
 * Time: 00:41
 */

namespace TyMappingMerge;


class MappingArrayVar extends MappingVarAbstract
{
    public function getValue($field)
    {
        if(is_array($this->var) && isset($this->var[$field])) {
            return $this->var[$field];
        }
        return false;
    }

    public function setValue($field, $value)
    {
        if(is_array($this->var)) {
            $this->var[$field] = $value;
        }
        return $this->var;
    }

    public function addValue($field, $value)
    {
        if(is_array($this->var) && $value) {
            $this->var[$field][] = $value;
        }
        return $this->var;
    }

    public function getWithSelect($selects = array())
    {
        if(count($selects) == 0) {
            return $this->getVar();
        }

        $tmpVar = $this->getVar();

        foreach ($tmpVar as $key => $value) {
            if(!in_array($key, $selects)) {
                unset($tmpVar[$key]);
            }
        }
        return $tmpVar;
    }

}