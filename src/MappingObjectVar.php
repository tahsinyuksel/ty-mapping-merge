<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/05/18
 * Time: 00:35
 */

namespace TyMappingMerge;


class MappingObjectVar extends MappingVarAbstract
{
    public function getValue($field)
    {
        $methodName = 'get' . ucfirst($field);
        if(method_exists($this->var, $methodName)) {
            return $this->var->$methodName();
        } else {
            return ($this->var->$field) ? $this->var->$field : false;
        }
    }

    public function setValue($field, $value)
    {
        $methodName = 'set' . ucfirst($field);
        if(method_exists($this->var, $methodName)) {
            $this->var->$methodName($value);
        } else {
            $this->var->$field = $value;
        }
        return $this->var;
    }

    public function addValue($field, $value)
    {
        $methodName = 'add' . ucfirst($field);
        if(method_exists($this->var, $methodName)) {
            $this->var->$methodName($value);
        } else {
            if(is_array($this->var->$field)) {
                $this->var->$field = array_push($this->var->$field, $value);    
            } else {
                $this->var->$field = array($value);
            }
        }
        return $this->var;
    }    

    public function getWithSelect($selects = array())
    {        
        if(count($selects) == 0) {
            return $this->getVar();
        }

        $tmpVar = $this->getVar();

        foreach (get_object_vars($tmpVar) as $key => $value) {
            if(!in_array($key, $selects)) {
                unset($tmpVar->$key);
            }
        }
        return $tmpVar;
    }

}